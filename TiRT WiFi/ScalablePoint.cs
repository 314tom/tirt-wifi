﻿using System.Windows;

namespace TiRT_WiFi
{
    public class ScalablePoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public ScalablePoint()
        {
        }

        public ScalablePoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static ScalablePoint FromPixels(double x, double y)
        {
            double diff = Settings.CurrentSettings.Diff;
            return new ScalablePoint(x / diff, y / diff);
        }
        
        public static ScalablePoint FromRealPoint(Point p)
        {
            double scale = Settings.CurrentSettings.Scale;
            return new ScalablePoint(p.X / scale, p.Y / scale);
        }

        public static ScalablePoint FromRealPoint(double x, double y)
        {
            double scale = Settings.CurrentSettings.Scale;
            return new ScalablePoint(x / scale, y / scale);
        }

        public Point GetPoint()
        {
            return new Point(X, Y);
        }

        public Point GetRealPoint()
        {
            double scale = Settings.CurrentSettings.Scale;
            return new Point(X * scale, Y * scale);
        }

        public Point GetPixelPoint()
        {
            double diff = Settings.CurrentSettings.Diff;
            return new Point(X * diff, Y * diff);
        }

        public override string ToString()
        {
            return $"Scalable point: {X} x {Y}";
        }

        public override bool Equals(object obj)
        {
            return obj is ScalablePoint && ((ScalablePoint) obj).X == X && ((ScalablePoint) obj).Y == Y;
        }
    }
}