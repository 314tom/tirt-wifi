﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using TiRT_WiFi.Model;

namespace TiRT_WiFi
{
    public class Utils
    {
        private static readonly int WallZIndex = 0;
        private static readonly int DeviceZIndex = 0;
        private static readonly int RouterZIndex = 50;
        private static readonly int HeatZIndex = -50;

        public static Canvas CreateMap(double width, double height, List<Router> routers,
            List<ReceivingDevice> receivingDevices, List<Wall> walls)
        {
            Canvas canvas = new Canvas
            {
                Width = width,
                Height = height,
                Background = Brushes.White
            };
            Size size = new Size(canvas.Width, canvas.Height);
            canvas.Measure(size);
            canvas.Arrange(new Rect(size));

            DrawHeatAndDevices(canvas, routers, receivingDevices, walls);
            DrawWalls(canvas, walls);
            return canvas;
        }

        public static string SaveCanvas(string filename, Canvas surface, string attr = "")
        {
            filename = filename.Replace(".png", "");
            filename = filename + " " + attr + "- " + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".png";
            string dir = Settings.GetDirectory() + @"\maps";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            Uri path = new Uri(dir + @"\" + filename);

            Transform transform = surface.LayoutTransform;
            surface.LayoutTransform = null;

            Size size = new Size(surface.Width, surface.Height);
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            RenderTargetBitmap renderBitmap =
                new RenderTargetBitmap(
                    (int) size.Width,
                    (int) size.Height,
                    96d,
                    96d,
                    PixelFormats.Pbgra32);
            renderBitmap.Render(surface);

            using (FileStream outStream = new FileStream(path.LocalPath, FileMode.Create))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                encoder.Save(outStream);
            }
            surface.LayoutTransform = transform;
            return path.AbsolutePath;
        }

        public static void DrawHeatAndDevices(Canvas canvas, List<Router> routers,
            List<ReceivingDevice> receivingDevices, List<Wall> walls)
        {
            DrawDevices(canvas, routers, receivingDevices);
            DrawHeat(canvas, routers, walls);
        }

        public static void DrawWalls(Canvas canvas, List<Wall> walls)
        {
            foreach (Line line in walls.Select(wall => wall.GetDisplayLine()).Select(displayLine => new Line
            {
                X1 = displayLine.X1,
                X2 = displayLine.X2,
                Y1 = displayLine.Y1,
                Y2 = displayLine.Y2,
                Stroke = displayLine.Stroke,
                StrokeThickness = displayLine.StrokeThickness
            }))
            {
                Panel.SetZIndex(line, WallZIndex);
                canvas.Children.Add(line);
            }
        }

        private static void DrawDevices(Canvas canvas, List<Router> routers, List<ReceivingDevice> receivingDevices)
        {
            List<Device> devices = new List<Device>();
            devices.AddRange(routers);
            devices.AddRange(receivingDevices);
            foreach (var device in devices)
            {
                var ellipse = GetEllipse(device.Location, 5, device is Router ? Brushes.Red : Brushes.Blue);
                Panel.SetZIndex(ellipse, device is Router ? RouterZIndex : DeviceZIndex);
                canvas.Children.Add(ellipse);
            }
        }

        private static Ellipse GetEllipse(ScalablePoint point, double radius, Brush color)
        {
            Ellipse ellipse = new Ellipse
            {
                Width = 2 * radius,
                Height = 2 * radius,
                Stroke = color,
                Fill = color
            };
            Canvas.SetLeft(ellipse, point.GetPixelPoint().X - radius);
            Canvas.SetTop(ellipse, point.GetPixelPoint().Y - radius);
            return ellipse;
        }

        private static void DrawHeat(Canvas canvas, List<Router> routers, List<Wall> walls)
        {
            double width = canvas.ActualWidth;
            double height = canvas.ActualHeight;
            double diff = Settings.CurrentSettings.Diff / 4;
            double maxHeat = double.MinValue;
//            double maxHeat = Tables.WiFiComfortTable[Tables.Value.NearRouter];
            double minHeat = Tables.WiFiComfortTable[Tables.Value.Noise];

            List<Tuple<double, ScalablePoint>> points = new List<Tuple<double, ScalablePoint>>();
            for (double positionY = 0; positionY <= height; positionY += diff)
            {
                for (double positionX = 0; positionX <= width; positionX += diff)
                {
                    ScalablePoint point = ScalablePoint.FromPixels(positionX, positionY);
                    if (routers.Any(r => r.Location.Equals(point)))
                        continue;
                    Logic logic = new Logic();
//                    double bestSignal = Math.Min(maxHeat, Math.Max(minHeat, logic.FindBestSignal(routers, point, walls.ToList())));
                    double bestSignal = Math.Max(minHeat, logic.FindBestSignal(routers, point, walls.ToList()));
                    points.Add(Tuple.Create(bestSignal, point));
                    maxHeat = Math.Max(maxHeat, bestSignal);
                }
            }

            foreach (Ellipse ellipse in points.Select(point => GetEllipse(point.Item2, 4,
                new SolidColorBrush(GetColor(minHeat, maxHeat, point.Item1)))))
            {
                Panel.SetZIndex(ellipse, HeatZIndex);
                canvas.Children.Add(ellipse);
            }
        }


        private static double Normalize(double min, double max, double value)
        {
            return (value - min) / (max - min);
        }

        private static byte Normalize(byte val1, byte val2, double proportions)
        {
            return (byte) (val1 * proportions + val2 * (1 - proportions));
        }

        private static Color GetColor(double min, double max, double value)
        {
            double mid = (min + max) / 2;
            Color green = Colors.Green;
            Color yellow = Colors.Yellow;
            Color red = Colors.Red;
            byte alpha = 160;
            if (value > mid)
            {
                var norm = Normalize(mid, max, value);
                return Color.FromArgb(alpha, Normalize(green.R, yellow.R, norm), Normalize(green.G, yellow.G, norm),
                    Normalize(green.B, yellow.B, norm));
            }
            else
            {
                var norm = Normalize(min, mid, value);
                return Color.FromArgb(alpha, Normalize(yellow.R, red.R, norm), Normalize(yellow.G, red.G, norm),
                    Normalize(yellow.B, red.B, norm));
            }
        }

        public static List<Wall> ReadWalls(string filename)
        {
            try
            {
                return ReadWalls(new FileStream(filename, FileMode.Open));
            }
            catch (Exception)
            {
                return new List<Wall>();
            }
        }

        public static List<Wall> ReadWalls(Stream stream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Wall>));
            return (List<Wall>) xmlSerializer.Deserialize(stream);
        }

        public static void SaveWalls(List<Wall> walls, Stream stream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Wall>));
            StreamWriter streamWriter = new StreamWriter(stream);
            xmlSerializer.Serialize(streamWriter, walls);
            streamWriter.Close();
        }
    }
}