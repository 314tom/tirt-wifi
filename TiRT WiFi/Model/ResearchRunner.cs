﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace TiRT_WiFi.Model
{
    public class ResearchRunner
    {
        public void Run()
        {
            string filename = Settings.GetDirectory() + @"\compiled " +
                              DateTime.Now.ToString("yyyy - MM - dd HH.mm.ss") + ".csv";
            File.Create(filename).Close();
            File.AppendAllLines(filename, new List<string> {ResearchOutput.Header()});
            var maps = new List<Tuple<List<Wall>, string, int, int>>();
//            maps.Add(Tuple.Create(new List<Wall>(), "empty"));
//            maps.Add(Tuple.Create(
//                Utils.ReadWalls(@"D:\Files\Dropbox\3.14Tom\Semestr 8\TiIRT\przykłady\Comarch.xml"),
//                "Comarch.xml", 1500, 500));
//            maps.Add(Tuple.Create(
//                Utils.ReadWalls(@"D:\Files\Dropbox\3.14Tom\Semestr 8\TiIRT\przykłady\centrala.xml"),
//                "centrala.xml", 800, 800));
            maps.Add(Tuple.Create(
                Utils.ReadWalls(@"D:\Files\Dropbox\3.14Tom\Semestr 8\TiIRT\przykłady\duży.xml"),
                "duży.xml", 1500, 1000));
            List<int> iterations = new List<int> {10, 100, 500, 1000, 2000};
            List<int> zasiegi = new List<int> {10, 20, 50, 100, 150, 200, 250};
            List<double> crossProbs = new List<double> {0.2, 0.5, 0.6, 0.7, 0.85, 1};
            List<double> mutationProbs = new List<double> {0.001, 0.002, 0.005, 0.01, 0.02, 0.035, 0.05};
            List<int> tournametSizes = new List<int> {2, 3, 5, 8, 10};
            List<int> routersNumbers = new List<int> {1, 2, 3, 4, 5};
            List<StopMode> methods = new List<StopMode> {StopMode.GoodSignal, StopMode.Cheap, StopMode.FitnessValue};
            foreach (var walls in maps)
            {
                int width = walls.Item3;
                int height = walls.Item4;
                Console.WriteLine(@"mapa: " + walls.Item2);
                Console.WriteLine(@"Wymiary rzeczywiste: " + ScalablePoint.FromPixels(width, height).GetRealPoint());
//                foreach (int iteration in iterations)
//                {
//                    Console.WriteLine(@"iterations: " + iteration);
//                    ResearchSettings rs =
//                        new ResearchSettings(mapName: walls.Item2, name: "iteracje", iterations: iteration,
//                            width: width, height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
//                foreach (int zasieg in zasiegi)
//                {
//                    Console.WriteLine(@"zasieg: " + zasieg);
//                    ResearchSettings rs =
//                        new ResearchSettings(mapName: walls.Item2, name: "zasieg", mutationRange: zasieg, width: width,
//                            height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
//                foreach (double crossProb in crossProbs)
//                {
//                    Console.WriteLine(@"crossProb: " + crossProb);
//                    ResearchSettings rs =
//                        new ResearchSettings(mapName: walls.Item2, name: "crossProb", crossProb: crossProb,
//                            width: width, height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
//                foreach (double mutationProb in mutationProbs)
//                {
//                    Console.WriteLine(@"mutationProb: " + mutationProb);
//                    ResearchSettings rs =
//                        new ResearchSettings(mapName: walls.Item2, name: "mutationProb", mutationProb: mutationProb,
//                            width: width, height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
//                foreach (int tournametSize in tournametSizes)
//                {
//                    Console.WriteLine(@"tournametSize: " + tournametSize);
//                    ResearchSettings rs = new ResearchSettings(mapName: walls.Item2, name: "tournamentSize",
//                        tournamentSize: tournametSize, width: width, height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
                foreach (StopMode method in methods)
                {
                    foreach (int routersNumber in routersNumbers)
                    {
                        Console.WriteLine(@"routersCount: " + routersNumber);
                        Console.WriteLine(@"method: " + method);
                        ResearchSettings rs = new ResearchSettings(mapName: walls.Item2, name: "routersCount+method",
                            method: method,
                            routers: routersNumber, width: width, height: height, devices: 500, populationSize: 100,
                            iterations: 500);
                        File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
                    }
                }
//                foreach (StopMode method in methods)
//                {
//                    Console.WriteLine(@"method: " + method);
//                    ResearchSettings rs = new ResearchSettings(mapName: walls.Item2, name: "method", method: method,
//                        width: width, height: height);
//                    File.AppendAllLines(filename, new List<string> {Research(walls.Item1, rs).ToString()});
//                }
            }
        }

        public void ResearchOnce(List<Router> routers, List<Wall> walls, List<ReceivingDevice> receivingDevices,
            int width, int height)
        {
            new GeneticAlgorithm(routers, receivingDevices, walls, 1000, width, height, 1500).RunAlgorithm(1000);
        }

        public ResearchOutput Research(List<Wall> walls, ResearchSettings rs)
        {
            return Research(GenerateRouters(rs.Routers), walls, rs);
        }

        public ResearchOutput Research(List<Router> routers, List<Wall> walls, ResearchSettings rs)
        {
            string name = rs.Name;
            if (name != "")
                name = name + " ";
            List<ResearchOutput> researchOutputs = new List<ResearchOutput>();
            List<ReceivingDevice> receivingDevices = GenerateDevices(rs.Devices, rs.Width, rs.Height);
            var csv = new StringBuilder();

            csv = headlineCSV(rs.MutationProb, rs.CrossProb, rs.MutationRange, rs.IsTournament, rs.TournamentSize, csv);
            for (int i = 0; i < rs.Repeats; i++)
            {
                GeneticAlgorithm ga = new GeneticAlgorithm(routers, receivingDevices, walls, rs.PopulationSize,
                    rs.Width, rs.Height, rs.DesiredFitness, rs.MutationProb, rs.CrossProb, rs.IsTournament,
                    rs.TournamentSize, rs.MutationRange);
                Individual best = ga.RunAlgorithm(rs.Iterations, rs.Method);
                ga.CalculateFitness(best.Genome);
//                Console.WriteLine(GA);
//                PrintListOfReceivers(GA.ReceivingDevices);
//                Console.WriteLine(GetAverageSignalValue(receivingDevices));
                string map = Utils.SaveCanvas("map " + name,
                    Utils.CreateMap(rs.Width, rs.Height, ga.Routers, ga.ReceivingDevices, walls));
                csv = addCSV(ga.ReceivingDevices, ga.Routers, csv, map, ga.IterationsCompleted);
                researchOutputs.Add(new ResearchOutput(ga.ReceivingDevices.Select(rd => rd.CurrentSignalReceived)
                    .ToList(), rs, ga.IterationsCompleted));
            }
            string filePath = Settings.GetDirectory() + @"\" + name + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") +
                              ".csv";

            File.WriteAllText(filePath, csv.ToString());
            Console.Beep(5000, 1000);
            return new ResearchOutput(researchOutputs);
        }

        private StringBuilder headlineCSV(double mutProb, double crossProb, int mutRange, bool isTournament,
            int tournamentSize, StringBuilder csv)
        {
            var newLine =
                $"mutationProbability: {mutProb}; crossoverProbability: {crossProb}; mutationRange: {mutRange}; isTournament: {isTournament}; tournamentSize: {tournamentSize}";
            csv.AppendLine(newLine);
            return csv;
        }

        private StringBuilder addCSV(List<ReceivingDevice> list, List<Router> routers, StringBuilder csv, string map,
            int iterations)
        {
            var newLine = $"{iterations};";
            foreach (Router router in routers)
            {
                newLine +=
                    $"Freq: {router.Frequency}; Power: {router.Power}; Location: {router.Location.GetRealPoint().X}x{router.Location.GetRealPoint().Y}; ;";
            }

            foreach (ReceivingDevice receivingDevice in list)
            {
                newLine += $"{receivingDevice.CurrentSignalReceived};";
            }
            newLine += $"{map};";
            newLine.Remove(newLine.Length - 1);
            csv.AppendLine(newLine);
            return csv;
        }

        private void PrintListOfReceivers(List<ReceivingDevice> list)
        {
            foreach (ReceivingDevice receivingDevice in list)
            {
                Console.WriteLine(receivingDevice);
            }
        }

        private static List<ReceivingDevice> GenerateDevices(int number, int width, int height)
        {
            Random random = new Random();
            List<ReceivingDevice> receivingDevices = new List<ReceivingDevice>();
            for (int i = 0; i < number; i++)
            {
                receivingDevices.Add(new ReceivingDevice("d" + i, 14, 10,
                    ScalablePoint.FromPixels((double) random.Next(0, width * 10 + 1) / 10,
                        (double) random.Next(0, height * 10 + 1) / 10)));
            }
            return receivingDevices;
        }

        private static double GetAverageSignalValue(List<ReceivingDevice> list)
        {
            return list.Sum(receivingDevice => receivingDevice.CurrentSignalReceived) / list.Count;
        }

        private static List<Router> GenerateRouters(int count)
        {
            List<Router> routers = new List<Router>();
            for (int i = 0; i < count; i++)
            {
                routers.Add(new Router("r1", 2400, 0, 3, new Point(0.5, 0.5)));
            }
            return routers;
        }
    }
}