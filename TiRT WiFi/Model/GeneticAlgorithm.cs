﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TiRT_WiFi.Model
{
    public class GeneticAlgorithm
    {
        public List<Individual> Population { get; set; }
        private static Random random;
        public Logic _logic = new Logic();
        private readonly List<ReceivingDevice> _receivingDevices;
        private readonly List<Wall> _walls;
        private readonly List<Router> _routers;
        public List<Router> Routers => _routers;
        public List<ReceivingDevice> ReceivingDevices => _receivingDevices;

        public static int _maxX, _maxY, _size;
        public static double _desiredFitness;
        private static double _mutationProbability;
        private static double _crossoverProbability;

        public delegate void SelectionDelegate();

        public SelectionDelegate _selectionDelegate;
        public int IterationsCompleted = 0;
        private readonly bool _isTournament;


        private static int _mutationRange, _tournamentSize;

        public GeneticAlgorithm(List<Router> routers, List<ReceivingDevice> recivers, List<Wall> walls,
            int populationSize, int x, int y, double desiredFitness, double mutationProb = 0.05,
            double crossProb = 0.85, bool isTournament = true, int tournamentSize = 5, int mutationRange = 100)
        {
            _routers = routers;
            _receivingDevices = recivers;
            _walls = walls;

            _size = populationSize;
            _maxX = x;
            _maxY = y;
            _desiredFitness = desiredFitness;
            _mutationProbability = mutationProb;
            _crossoverProbability = crossProb;
            _isTournament = isTournament;
            _tournamentSize = tournamentSize;
            _mutationRange = mutationRange;
            SetSelection(_isTournament);

            random = new Random();
            Population = new List<Individual>();

            for (int i = 0; i < _size; i++)
            {
                Individual ind = new Individual(_routers.Count, _maxX, _maxY, random.Next());
                AddIndividualToPopulation(ind);
            }
        }

        public Tuple<double, double, int, int, bool, int> GetArguments()
        {
            return new Tuple<double, double, int, int, bool, int>(_mutationProbability, _crossoverProbability,
                _mutationRange, IterationsCompleted, _isTournament, _tournamentSize);
        }

        #region Algorithm

        public Individual RunAlgorithm(int iterations = 10000, StopMode method = StopMode.FitnessValue)
        {
            bool notFound = true;
            int iter = 0;
            Individual best = new Individual(1, 1, 1, 1) {FitnessValue = Double.MaxValue};
            for (int i = 0; i < iterations; i++)
            {
                if (i % 10 == 0)
                    Console.Write(@"_");
            }
            Console.WriteLine();
            while (notFound && iter < iterations)
            {
//               Console.WriteLine(best.FitnessValue + ", iterations: " + iter);
                if (iter % 10 == 0)
                    Console.Write(@"#");
                Iterate();
                notFound = Evaluate(method);
                iter++;


                Individual currentBest = FindBestIndividual();
                if (currentBest.FitnessValue < best.FitnessValue)
                    best = currentBest;
            }
            IterationsCompleted = iter;
            Console.WriteLine();
            Console.WriteLine("BEST: " + best.FitnessValue + ", iterations: " + iter);

            return best;
        }

        private void Iterate()
        {
            while (Population.Count > _size * 0.6)
                Selection();
            while (Population.Count < _size)
                Crossover();
            Mutate();
        }

        private void AddIndividualToPopulation(Individual ind)
        {
            ind.FitnessValue = CalculateFitness(ind.Genome);
            Population.Add(ind);
        }

        #endregion

        private void Crossover()
        {
            //losowanie pary do krzyżowania
            int firstIndividualIndex = random.Next(0, Population.Count),
                secondIndividualIndex = random.Next(0, Population.Count);
            int firstSeparatorPoint = 0, secondSeparatorPoint = 0;
            while (secondIndividualIndex == firstIndividualIndex)
            {
                secondIndividualIndex = random.Next(0, Population.Count);
            }

            //czy nastąpi wymiana genów, czy powstaną jedynie kopie rodziców
            int genomeSize = Population[0].GenomeSize;
            if (random.NextDouble() < _crossoverProbability)
            {
                firstSeparatorPoint = random.Next(0, genomeSize);
                secondSeparatorPoint = random.Next(firstSeparatorPoint + 1, genomeSize + 1);
            }

            //tworzenie dzieci jako kopia rodziców
            int[] child = new int[genomeSize];
            int[] child2nd = new int[genomeSize];
            Population[firstIndividualIndex].Genome.CopyTo(child, 0);
            Population[secondIndividualIndex].Genome.CopyTo(child2nd, 0);

            //wymiana genów
            for (int i = firstSeparatorPoint; i < secondSeparatorPoint; i++)
            {
                child[i] = Population[secondIndividualIndex].Genome[i];
                child2nd[i] = Population[firstIndividualIndex].Genome[i];
            }

            //dodanie do populacji nowopowstałych dzieci
            AddIndividualToPopulation(new Individual(child));
            AddIndividualToPopulation(new Individual(child2nd));
        }

        #region Selection

        public void SetSelection(bool isTournament)
        {
            if (isTournament)
                _selectionDelegate = Tournament;
            else
                _selectionDelegate = Ranking;
        }

        private void Selection()
        {
            _selectionDelegate();
        }

        private void Ranking()
        {
            throw new NotImplementedException();
        }

        private void Tournament()
        {
            int temp;
            List<Individual> tour = new List<Individual>();
            while (tour.Count < _tournamentSize)
            {
                temp = random.Next(0, Population.Count);
                if (!tour.Contains(Population[temp]))
                {
                    tour.Add(Population[temp]);
                }
            }
            Individual winner = FittestIndividual(tour);
            tour.Remove(winner);

            foreach (Individual t in tour)
            {
                Population.Remove(t);
            }

//            Console.Write("-");
        }

        private Individual FittestIndividual(List<Individual> tour)
        {
            Individual best = tour[0];
            double bestfitness = best.FitnessValue;
            foreach (Individual ind in tour)
            {
                if (ind.FitnessValue < bestfitness)
                {
                    bestfitness = ind.FitnessValue;
                    best = ind;
                }
            }
            return best;
        }

        #endregion

        #region Mutation

        private void Mutate()
        {
            foreach (Individual individual in Population)
            {
                for (int i = 0; i < individual.GenomeSize; i++)
                {
                    bool wasMutated = false;
                    if (random.NextDouble() < _mutationProbability)
                    {
                        wasMutated = true;
                        if (i % 2 == 0)
                            individual.Genome[i] += GeneMutation(individual.Genome[i], _maxX);
                        else
                            individual.Genome[i] += GeneMutation(individual.Genome[i], _maxX);
                    }
                    if (wasMutated)
                        individual.FitnessValue = CalculateFitness(individual.Genome);
                }
            }
        }

        private int GeneMutation(int value, int range)
        {
            int result = random.Next(0, 2 * _mutationRange + 1) - _mutationRange;
            while (value + result < 0 || value + result > range)
            {
                result = random.Next(0, 2 * _mutationRange + 1) - _mutationRange;
            }
            return result;
        }

        #endregion

        #region Evaluation

        //mówi, czy mamy kontynuować iteracje (zwraca false, jak już mamy dostatecznie dobrego osobnika)
        private bool Evaluate(StopMode method)
        {
            switch (method)
            {
                //dobrze
                case StopMode.GoodSignal:
                    foreach (Individual individual in Population)
                    {
                        for (var i = 0; i < _routers.Count; i++)
                        {
                            Router router = _routers[i];
                            ScalablePoint sp =
                                ScalablePoint.FromPixels(individual.Genome[2 * i], individual.Genome[2 * i + 1]);
                            router.Location = sp;
                        }

                        _logic.CalculateSignals(_routers, _receivingDevices, _walls);

                        return _receivingDevices.Any(rd => rd.CurrentSignalReceived <
                                                    Tables.WiFiComfortTable[Tables.Value.AppsStreaming]);
                    }
                    return true;
                //tanio
                case StopMode.Cheap:
                    foreach (Individual individual in Population)
                    {
                        for (var i = 0; i < _routers.Count; i++)
                        {
                            Router router = _routers[i];
                            ScalablePoint sp =
                                ScalablePoint.FromPixels(individual.Genome[2 * i], individual.Genome[2 * i + 1]);
                            router.Location = sp;
                        }

                        _logic.CalculateSignals(_routers, _receivingDevices, _walls);

                        return _receivingDevices.Any(rd => rd.CurrentSignalReceived <
                                                           Tables.WiFiComfortTable[Tables.Value.WwwEmail]);
                    }
                    return true;
                //domyślnie
                default:
                    if (Population.Any(individual => individual.FitnessValue <= _desiredFitness))
                    {
                        return false;
                    }
                    return true;
            }
        }

        public double CalculateFitness(int[] genome)
        {
            double fitness = 0;
            //przestaw routery
            for (var i = 0; i < _routers.Count; i++)
            {
                Router router = _routers[i];
                //router.Location = new ScalablePoint(genome[2*i]/100d, genome[2*i + 1]/100d);
                ScalablePoint sp = ScalablePoint.FromPixels(genome[2 * i], genome[2 * i + 1]);
                router.Location = sp;
            }

            //przelicz sygnał odbierany przez urządzenia z routerów
            _logic.CalculateSignals(_routers, _receivingDevices, _walls);
            //zsumuj kwadrat sygnału

            foreach (ReceivingDevice receivingDevice in _receivingDevices)
            {
                fitness += receivingDevice.CurrentSignalReceived * receivingDevice.CurrentSignalReceived;
            }
            return fitness;
        }

        private Individual FindBestIndividual()
        {
            double currentFitnessMin = double.MaxValue;
            int currentIndex = -1;
            for (var i = 0; i < Population.Count; i++)
            {
                Individual ind = Population[i];
                if (ind.FitnessValue <= currentFitnessMin)
                {
                    currentFitnessMin = ind.FitnessValue;
                    currentIndex = i;
                }
            }
            return Population[currentIndex];
        }

        #endregion

        public override string ToString()
        {
            string s = "POPULACJA:";
            foreach (Individual individual in Population)
            {
                s += "\n" + individual.ToString();
            }
            return s;
        }
    }
}