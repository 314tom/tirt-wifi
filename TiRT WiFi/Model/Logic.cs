﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace TiRT_WiFi.Model
{
    public class Logic
    {

        #region Signal

        public double FreeSpacePathLoss(Router router, ScalablePoint device)
        {
            Point routerPoint = router.Location.GetRealPoint();
            Point devicePoint = device.GetRealPoint(); 
            double x = routerPoint.X - devicePoint.X;
            double y = routerPoint.Y - devicePoint.Y;
            double d = Math.Sqrt(x*x + y*y);
            return 20 * Math.Log10(d) + 20 * Math.Log10(router.Frequency) - 27.55;
        }

        public double Friis(Router router, ReceivingDevice device)
        {
            return router.Power + router.Gain + device.Gain - FreeSpacePathLoss(router, device.Location);
        }

        //zwraca siłę sygnału (Friss - przeszkody)
        public double GetSignalStrength(Router router, ReceivingDevice device, List<Wall> intersectedWalls)
        {
            double sumOfObstacles = 0;
            foreach (Wall wall in intersectedWalls)
            {
                sumOfObstacles += wall.GetMaterial().Value;
            }
            return Friis(router, device)-sumOfObstacles;
        }

        //zwraca wartość najlepszego sygnału odbieranego przez urządzenie ze wszystkich routerów
        public double FindBestSignal(List<Router> routers, ReceivingDevice device, List<Wall> walls)
        {
            double best = Double.MinValue;
            foreach (Router router in routers)
            {
                List<Wall> intersectedWalls =
                    GetIntersectingWallsBetweenPoints(walls, router.Location, device.Location);
                double signal = GetSignalStrength(router, device, intersectedWalls);
                if (signal > best)
                    best = signal;
            }
            return best;
        }

        //oblicza najlepszą siłę sygnału odbieranego przez każde urządzenie
        public void CalculateSignals(List<Router> routers, List<ReceivingDevice> devices, List<Wall> walls)
        {
            foreach (ReceivingDevice device in devices)
            {
                device.CurrentSignalReceived = FindBestSignal(routers, device, walls);
            }
        }
        #endregion

        #region Obstacles
        public List<Wall> GetIntersectingWallsBetweenPoints(List<Wall> walls, ScalablePoint routerLocation, ScalablePoint deviceLocation)
        {
            var line = Tuple.Create(routerLocation.GetRealPoint(), deviceLocation.GetRealPoint());
            return (from wall in walls where Intersects(line, wall.GetRealLine()) select wall).ToList();
        }

        private static bool Intersects(Tuple<Point, Point> line1, Tuple<Point, Point> line2)
        {
            Point p1 = line1.Item1;
            Point p2 = line1.Item2;
            Point p3 = line2.Item1;
            Point p4 = line2.Item2;
            Vector p21 = p2 - p1;
            Vector p31 = p3 - p1;
            Vector p41 = p4 - p1;
            Vector p23 = p2 - p3;
            Vector p43 = p4 - p3;
            Vector p13 = p1 - p3;
            double p21Xp31 = IloczynWektorowy(p21, p31);
            double p21Xp41 = IloczynWektorowy(p21, p41);
            double p43Xp23 = IloczynWektorowy(p43, p23);
            double p43Xp13 = IloczynWektorowy(p43, p13);
            var intersects = p21Xp31 * p21Xp41 <= 0 && p43Xp23 * p43Xp13 <= 0;
            if (p21Xp31 == 0 && p21Xp41 == 0 && p43Xp23 == 0 && p43Xp13 == 0)
            {
                List<Point> points = new List<Point>
                {
                    p1,
                    p2,
                    p3,
                    p4
                };
                points.Sort((point1, point2) =>
                {
                    var compareTo = point1.X.CompareTo(point2.X);
                    if (compareTo == 0)
                        compareTo = point1.Y.CompareTo(point2.Y);
                    return compareTo;
                });
                return !((points[0].Equals(p1) || points[0].Equals(p2)) &&
                         (points[1].Equals(p1) || points[1].Equals(p2))) &&
                       !((points[0].Equals(p3) || points[0].Equals(p4)) &&
                         (points[1].Equals(p3) || points[1].Equals(p4))) ||
                       points[1].X == points[2].X && points[1].Y == points[2].Y;
            }
            return intersects;
        }

        private static double IloczynWektorowy(Vector p1, Vector p2)
        {
            return p1.X * p2.Y - p1.Y * p2.X;
        }

        #endregion

        #region SignalInPoint

        public double Friis(Router router, ScalablePoint device)
        {
            return router.Power + router.Gain - FreeSpacePathLoss(router, device);
        }

        //zwraca siłę sygnału (Friss - przeszkody)
        public double GetSignalStrength(Router router, ScalablePoint device, List<Wall> walls)
        {
            List<Wall> intersectingWalls = GetIntersectingWallsBetweenPoints(walls, router.Location, device);
            double sumOfObstacles = 0;
            foreach (Wall wall in intersectingWalls)
            {
                sumOfObstacles += wall.GetMaterial().Value;
            }
            return Friis(router, device) - sumOfObstacles;
        }

        //zwraca wartość najlepszego sygnału odbieranego przez urządzenie ze wszystkich routerów
        public double FindBestSignal(List<Router> routers, ScalablePoint device, List<Wall> walls)
        {
            double best = Double.MinValue;
            foreach (Router router in routers)
            {
                double signal = GetSignalStrength(router, device, walls);
                if (signal > best)
                    best = signal;
            }
            return best;
        }


        #endregion
    }
}
