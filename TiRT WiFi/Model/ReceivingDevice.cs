﻿using System;
using System.Windows;

namespace TiRT_WiFi.Model
{
    public class ReceivingDevice : Device
    {
        public double Gain { get; } //[dB]
        public double Power { get; } //[dB]
        public int Id { get; set; }
        public string Type { get; }
        public double CurrentSignalReceived { get; set; }

        public ReceivingDevice(string name, double gain, double power, ScalablePoint p, int id = -1, string type = "computer") : base(name, p)
        {
            Gain = gain;
            Power = power;
            Id = id;
            Type = type;
            CurrentSignalReceived = Double.MinValue;
        }

        public ReceivingDevice(string name, double gain, double power, Point p, int id = -1, string type = "computer") : base(name, p)
        {
            Gain = gain;
            Power = power;
            Id = id;
            Type = type;
            CurrentSignalReceived = Double.MinValue;
        }

        public override string ToString()
        {
            return Name + "-G: " + Gain + ", TP: " + Power + "; "+Type + "; Signal: " + CurrentSignalReceived + " dB.";
        }
    }
}
