﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TiRT_WiFi.Model
{
    public class ResearchOutput
    {
        public ResearchSettings ResearchSettings { get; }
        public double Min { get; }
        public double Max { get; }
        public double Median { get; }
        public double Mean { get; }
        public double Variance { get; }
        private double StandardDeviation => Math.Sqrt(Variance);
        public double Group1 { get; }
        public double Group2 { get; }
        public double Group3 { get; }
        public double Group4 { get; }
        public double Group5 { get; }
        public double Group6 { get; }
        public double Iterations { get; }

        public ResearchOutput(List<double> signals, ResearchSettings researchSettings, int iterations)
        {
            ResearchSettings = researchSettings;
            signals.Sort();
            int size = signals.Count;
            Min = signals[0];
            Max = signals[size - 1];
            Median = (signals[size / 2] + signals[(size - 1) / 2]) / 2;
            Mean = signals.Sum() / size;
            Variance = signals.Sum(t => Math.Pow(t - Mean, 2)) / size;
            Iterations = iterations;

            foreach (var value in signals)
            {
                if (value < Tables.WiFiComfortTable[Tables.Value.Noise]) Group1++;
                else if (value < Tables.WiFiComfortTable[Tables.Value.Ping]) Group2++;
                else if (value < Tables.WiFiComfortTable[Tables.Value.WwwEmail]) Group3++;
                else if (value < Tables.WiFiComfortTable[Tables.Value.AppsStreaming]) Group4++;
                else if (value < Tables.WiFiComfortTable[Tables.Value.NearRouter]) Group5++;
                else Group6++;
            }
        }

        public ResearchOutput(List<ResearchOutput> outputs)
        {
            if(outputs.Count==0)
                throw new Exception("Empty data");
            if(!outputs.TrueForAll(r => r.ResearchSettings.Equals(outputs[0].ResearchSettings)))
                throw new Exception("Different settings");
            ResearchSettings = outputs[0].ResearchSettings;
            Min = outputs.Select(o => o.Min).Average();
            Max = outputs.Select(o => o.Max).Average();
            Mean = outputs.Select(o => o.Median).Average();
            Mean = outputs.Select(o => o.Mean).Average();
            Variance = outputs.Select(o => o.Variance).Average();
            Group1 = outputs.Select(o => o.Group1).Average();
            Group2 = outputs.Select(o => o.Group2).Average();
            Group3 = outputs.Select(o => o.Group3).Average();
            Group4 = outputs.Select(o => o.Group4).Average();
            Group5 = outputs.Select(o => o.Group5).Average();
            Group6 = outputs.Select(o => o.Group6).Average();
            Iterations = outputs.Select(o => o.Iterations).Average();
        }

        public static string Header()
        {
            const string sep = ";";
            return ResearchSettings.Header() +
                   $"{sep}min{sep}max{sep}median{sep}mean{sep}variance{sep}standardDeviation{sep}group1{sep}group2{sep}group3{sep}group4{sep}group5{sep}group6{sep}iterations{sep}";
        }

        public override string ToString()
        {
            const string sep = ";";
            return
                $"{ResearchSettings}{sep}{Min}{sep}{Max}{sep}{Median}{sep}{Mean}{sep}{Variance}{sep}{StandardDeviation}{sep}{Group1}{sep}{Group2}{sep}{Group3}{sep}{Group4}{sep}{Group5}{sep}{Group6}{sep}{Iterations}{sep}";
        }
    }
}