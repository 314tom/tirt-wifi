﻿using System.Collections.Generic;

namespace TiRT_WiFi.Model
{
    public class Tables
    {
        public static Dictionary<Value, double> WiFiComfortTable { get; } = new Dictionary<Value, double>
        {
            {Value.NearRouter, -30},
            {Value.AppsStreaming, -67},
            {Value.WwwEmail, -70},
            {Value.Ping, -80},
            {Value.Noise, -90}
        };

        public enum Value
        {
            NearRouter,
            AppsStreaming,
            WwwEmail,
            Ping,
            Noise
        }
    }
}