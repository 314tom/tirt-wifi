﻿namespace TiRT_WiFi.Model
{
    public class ResearchSettings
    {
        public int Repeats { get; }
        public int Routers { get; }
        public int Devices { get; }
        public int PopulationSize { get; }
        public double DesiredFitness { get; }
        public string MapName { get; }
        public string Name { get; }
        public int Width { get; }
        public int Height { get; }
        public int Iterations { get; }
        public double MutationProb { get; }
        public double CrossProb { get; }
        public bool IsTournament { get; }
        public int TournamentSize { get; }
        public int MutationRange { get; }
        public StopMode Method { get; }


        public ResearchSettings(int repeats = 3, int routers = 3, int devices = 1000, int populationSize = 200,
            double desiredFitness = 1500, string mapName = "", string name = "", int width = 500, int height = 500,
            int iterations = 1000, double mutationProb = 0.05, double crossProb = 0.85, bool isTournament = true,
            int tournamentSize = 5, int mutationRange = 100, StopMode method = StopMode.FitnessValue)
        {
            Repeats = repeats;
            Routers = routers;
            Devices = devices;
            PopulationSize = populationSize;
            DesiredFitness = desiredFitness;
            MapName = mapName;
            Name = name;
            Width = width;
            Height = height;
            Iterations = iterations;
            MutationProb = mutationProb;
            CrossProb = crossProb;
            IsTournament = isTournament;
            TournamentSize = tournamentSize;
            MutationRange = mutationRange;
            Method = method;
        }


        public static string Header()
        {
            const string sep = ";";
            return
                $"Repeats{sep}Routers{sep}Devices{sep}PopulationSize{sep}DesiredFitness{sep}Method{sep}MapName{sep}Name{sep}Width{sep}Height{sep}Iterations{sep}MutationProb{sep}CrossProb{sep}IsTournament{sep}TournamentSize{sep}MutationRange";
        }

        public override string ToString()
        {
            const string sep = ";";
            return
                $"{Repeats}{sep}{Routers}{sep}{Devices}{sep}{PopulationSize}{sep}{DesiredFitness}{sep}{Method}{sep}{MapName}{sep}{Name}{sep}{Width}{sep}{Height}{sep}{Iterations}{sep}{MutationProb}{sep}{CrossProb}{sep}{IsTournament}{sep}{TournamentSize}{sep}{MutationRange}";
        }

        protected bool Equals(ResearchSettings other)
        {
            return Repeats == other.Repeats && Routers == other.Routers && Devices == other.Devices &&
                   PopulationSize == other.PopulationSize &&
                   DesiredFitness.Equals(other.DesiredFitness) && Method.Equals(other.Method) &&
                   string.Equals(MapName, other.MapName) &&
                   string.Equals(Name, other.Name) && Width == other.Width && Height == other.Height &&
                   Iterations == other.Iterations && MutationProb.Equals(other.MutationProb) &&
                   CrossProb.Equals(other.CrossProb) && IsTournament == other.IsTournament &&
                   TournamentSize == other.TournamentSize && MutationRange == other.MutationRange;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ResearchSettings) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Repeats;
                hashCode = (hashCode * 397) ^ Devices;
                hashCode = (hashCode * 397) ^ PopulationSize;
                hashCode = (hashCode * 397) ^ DesiredFitness.GetHashCode();
                hashCode = (hashCode * 397) ^ (MapName != null ? MapName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Width;
                hashCode = (hashCode * 397) ^ Height;
                hashCode = (hashCode * 397) ^ Iterations;
                hashCode = (hashCode * 397) ^ MutationProb.GetHashCode();
                hashCode = (hashCode * 397) ^ CrossProb.GetHashCode();
                hashCode = (hashCode * 397) ^ IsTournament.GetHashCode();
                hashCode = (hashCode * 397) ^ TournamentSize;
                hashCode = (hashCode * 397) ^ MutationRange;
                return hashCode;
            }
        }
    }
}