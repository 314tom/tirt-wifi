﻿using System;

namespace TiRT_WiFi.Model
{
    public class Individual
    {
        //private List<Position> routers;
        public int GenomeSize => Genome.Length;
        //private int divider;
        
        public int[] Genome { get; set; }

        public double FitnessValue { get; set; }


        //public Individual(int[] g, double f)
        //{
        //    Genome = g;
        //    FitnessValue = f;
        //}
        public Individual(int[] g)
        {
            Genome = g;
            //calculateFitness();
        }

        //przy tworzeniu nowego osobnika
        public Individual(int numberOfRouters, int maxX, int maxY, int seed)
        {
            Random ran = new Random(seed);
            Genome = new int[2* numberOfRouters];
            for (int i = 0; i < numberOfRouters; i++)
            {
                Genome[2*i] = ran.Next(0, maxX+1);
                Genome[2*i + 1] = ran.Next(0, maxY + 1);
            }
            //calculateFitness();
        }

        public Individual(int numberOfRouters, int maxX_Y, int salt) : this(numberOfRouters, maxX_Y, maxX_Y, salt) {}
        
        //public void calculateFitness()
        //{
        //    //TODO: liczenie fitnessu!
        //    FitnessValue = double.MaxValue;
        //}
        
        public override string ToString()
        {
            string s = "[ ";
            string g;
            for (int i = 0; i < Genome.Length/2; i++)
            {
                g = "(" + Genome[2*i];
                s += g.PadLeft(4, ' ') + ",";
                g = "" + Genome[2*i + 1];
                s += g.PadLeft(4, ' ') + "), ";
            }
            s.Remove(s.Length - 2);
            s += " ],    fitness: " + FitnessValue;
            return s;
        }
    }


    //public struct Position
    //{
    //    public int X { get; set; }
    //    public int Y { get; set; }

    //    public Position(int x, int y)
    //    {
    //        X = x;
    //        Y = y;
    //    }
    //}
}