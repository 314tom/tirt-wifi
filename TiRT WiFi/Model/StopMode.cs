﻿namespace TiRT_WiFi.Model
{
    public enum StopMode
    {
        GoodSignal,
        Cheap,
        FitnessValue
    }
}
