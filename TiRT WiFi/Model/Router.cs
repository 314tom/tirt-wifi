﻿using System.Windows;

namespace TiRT_WiFi.Model
{
    public class Router : Device
    {
        public int Frequency { get; }
        public double Gain { get; } //[dB]
        public double Power { get; } //[dB]
        public int Id { get; set; }

        public Router(string name, int freq, double gain, double power, ScalablePoint p, int id=-1) : base(name, p)
        {
            Frequency = freq;
            Gain = gain;
            Power = power;
            Id = id;
        }

        public Router(string name, int freq, double gain, double power, Point p, int id=-1) : base(name, p)
        {
            Frequency = freq;
            Gain = gain;
            Power = power;
            Id = id;
        }
        
        public override string ToString()
        {
            return Name+"-G: "+Gain + ", TP: "+Power;
        }
    }
}
