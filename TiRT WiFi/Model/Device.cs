﻿using System.Windows;

namespace TiRT_WiFi.Model
{
    public abstract class Device
    {
        public string Name { get; protected set; }

        public ScalablePoint Location { get; set; } //[m,m]

        protected Device(string name, Point p)
        {
            Name = name;
            Location = ScalablePoint.FromRealPoint(p);
        }

        protected Device(string name, ScalablePoint p)
        {
            Name = name;
            Location = p;
        }
    }
}