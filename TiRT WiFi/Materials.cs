﻿using System.Collections.Generic;
using System.Windows.Media;

namespace TiRT_WiFi
{
    public static class Materials
    {
        public static List<Material> MaterialsList { get; } = new List<Material>
        {
            new Material("cegła 90 mm", 3.5, Colors.IndianRed),
            new Material("cegła 190 mm", 5, Colors.Red),
            new Material("cegła 270 mm", 7, Colors.DarkRed),
            new Material("marmur", 5, Colors.Violet),
            new Material("szkło 6mm", 0.8, Colors.LightBlue),
            new Material("szkło 13mm", 2, Colors.DodgerBlue),
            new Material("szkło biurowe 13mm", 3, Colors.Blue),
            new Material("kamień 200 mm", 12, Colors.LimeGreen),
            new Material("kamień 400 mm", 17, Colors.ForestGreen),
            new Material("kamień 600 mm", 28, Colors.DarkGreen),
            new Material("beton 100 mm", 12, Colors.DarkGray),
            new Material("beton 200 mm", 23, Colors.DimGray),
            new Material("beton 300 mm", 35, Colors.Black),
            new Material("wzmocniony beton 90 mm", 27, Colors.DarkSlateGray),
            new Material("płyta gipsowa", 3, Colors.Purple),
            new Material("drzwi drewniane", 3, Colors.Pink),
            new Material("drzwi metalowe", 6, Colors.DeepPink)
        };
    }
}