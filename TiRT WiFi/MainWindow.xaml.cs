﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;
using TiRT_WiFi.Model;

namespace TiRT_WiFi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly ObservableCollection<Wall> _walls = new ObservableCollection<Wall>();
        private readonly List<Material> _materials = Materials.MaterialsList;
        private readonly List<Router> _routers = new List<Router>();
        private readonly List<ReceivingDevice> _receivingDevices = new List<ReceivingDevice>();

        #region TemporaryValues

        private Point? _lastStart;

        private readonly Line _tempLine = new Line
        {
            StrokeThickness = Settings.CurrentSettings.StrokeThickness
        };

        #endregion


        public MainWindow()
        {
            _routers.Add(new Router("r1", 2400, 0, 3, new Point(0.5, 0.5)));
            _routers.Add(new Router("r2", 2400, 0, 3, new Point(2, 18)));
            _routers.Add(new Router("r3", 2400, 0, 3, new Point(15, 18)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(10, 15)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(12, 5)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(15, 17)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(1, 1)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(8, 8)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(15, 2)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(4, 15)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(12, 19)));
            _receivingDevices.Add(new ReceivingDevice("d1", 14, 10, new Point(3, 17)));
            InitializeComponent();
            LoadSettings(Settings.CurrentSettings);
            MaterialsList.ItemsSource = _materials;
            WallsList.ItemsSource = _walls;
            _walls.CollectionChanged += _walls_CollectionChanged;
        }

        private void LoadSettings(Settings settings)
        {
            MapCreatorWidthSelector.Increment = settings.Diff;
            MapCreatorHeightSelector.Increment = settings.Diff;
            MapCreator.Width = settings.Width;
            MapCreator.Height = settings.Height;
            MapCreatorWidthSelector.Value = MapCreator.Width;
            MapCreatorHeightSelector.Value = MapCreator.Height;
        }

        private void _walls_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var newWall in e.NewItems)
                {
                    MapCreator.Children.Add(((Wall) newWall).GetDisplayLine());
                }
            }
            if (e.OldItems != null)
            {
                foreach (var removedWall in e.OldItems)
                {
                    MapCreator.Children.Remove(((Wall) removedWall).GetDisplayLine());
                }
            }
        }

        private void MapCreatorMouseUp(object sender, MouseButtonEventArgs e)
        {
            Point position = RoundToDiff(e.GetPosition(MapCreator), Settings.CurrentSettings.Diff);
            if (_lastStart == null || _lastStart.Value == position)
            {
                _lastStart = null;
                return;
            }
            MapCreator.Children.Remove(_tempLine);
            Wall wall = new Wall(MaterialsList.SelectedIndex, (Point) _lastStart, position);
            Console.WriteLine(wall.Point1.GetRealPoint() + "  -  " + wall.Point2.GetRealPoint());
            _walls.Add(wall);
            _lastStart = null;

            RedrawDevicesAndHeat();
        }

        private void MapCreatorOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            _lastStart = RoundToDiff(e.GetPosition(MapCreator), Settings.CurrentSettings.Diff);
            var color = _materials[MaterialsList.SelectedIndex].Color;
            _tempLine.Stroke = new SolidColorBrush(Color.FromArgb(127, color.R, color.G, color.B));
        }

        private void MapCreator_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_lastStart == null)
            {
                e.Handled = false;
                return;
            }
            MapCreator.Children.Remove(_tempLine);
            _tempLine.X1 = (double) _lastStart?.X;
            _tempLine.Y1 = (double) _lastStart?.Y;
            Point position = RoundToDiff(e.GetPosition(MapCreator), Settings.CurrentSettings.Diff);
            _tempLine.X2 = position.X;
            _tempLine.Y2 = position.Y;
            MapCreator.Children.Add(_tempLine);
        }

        private static double RoundToDiff(double value, double diff)
        {
            return Math.Round(value / diff) * diff;
        }

        private static Point RoundToDiff(Point point, double diff)
        {
            return new Point(RoundToDiff(point.X, diff), RoundToDiff(point.Y, diff));
        }

        private void WallsList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            foreach (var removed in e.RemovedItems)
            {
                ((Wall) removed).GetDisplayLine().StrokeDashArray = new DoubleCollection();
            }
            foreach (var added in e.AddedItems)
            {
                ((Wall) added).GetDisplayLine().StrokeDashArray = new DoubleCollection(new List<double> {5, 2});
            }
            RedrawDevicesAndHeat();
        }

        private void RemoveWall(object sender, RoutedEventArgs e)
        {
            List<Wall> temp = WallsList.SelectedItems.Cast<Wall>().ToList();
            temp.ForEach(wall => _walls.Remove(wall));
        }

        #region Mapa

        private void MapCreator_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MapCreator.Children.Clear();
            double width = MapCreator.ActualWidth;
            double height = MapCreator.ActualHeight;
            double diff = Settings.CurrentSettings.Diff;
            for (double position = diff; position < height; position += diff)
            {
                Line line = new Line
                {
                    X1 = 0,
                    X2 = width,
                    Y1 = position,
                    Y2 = position,
                    Stroke = new SolidColorBrush(Color.FromArgb(64, 0, 0, 0)),
                    SnapsToDevicePixels = true
                };
                MapCreator.Children.Add(line);
            }
            for (double position = diff; position < width; position += diff)
            {
                Line line = new Line
                {
                    Y1 = 0,
                    Y2 = height,
                    X1 = position,
                    X2 = position,
                    Stroke = new SolidColorBrush(Color.FromArgb(64, 0, 0, 0)),
                    SnapsToDevicePixels = true
                };
                MapCreator.Children.Add(line);
            }
            foreach (var line in _walls.Select(wall => wall.GetDisplayLine()))
            {
                MapCreator.Children.Add(line);
            }
            RedrawDevicesAndHeat();
            Console.WriteLine("Wymiary plansz [m]: " + ScalablePoint.FromPixels(width, height).GetRealPoint());
        }

        private void MapCreatorChangeHeight(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue != null)
                MapCreator.Height = (double) e.NewValue;
        }

        private void MapCreatorChangeWidth(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue != null)
                MapCreator.Width = (double) e.NewValue;
        }

        private void RedrawDevicesAndHeat()
        {
            for (int i = MapCreator.Children.Count - 1; i >= 0; i--)
            {
                if (MapCreator.Children[i] is Ellipse)
                    MapCreator.Children.RemoveAt(i);
            }
            Utils.DrawHeatAndDevices(MapCreator, _routers, _receivingDevices, _walls.ToList());
        }

        #endregion

        #region Przyciski

        private void CalculateButton(object sender, RoutedEventArgs e)
        {
            new ResearchRunner().ResearchOnce(_routers, _walls.ToList(), _receivingDevices, (int) MapCreator.ActualWidth, (int) MapCreator.ActualHeight);
            RedrawDevicesAndHeat();
        }

        private void SaveButton(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                AddExtension = true,
                Filter = "Plik XML (*.xml)|*.xml"
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                Utils.SaveWalls(_walls.ToList(), saveFileDialog.OpenFile());
            }
        }

        private void LoadButton(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Plik XML (*.xml)|*.xml",
                CheckPathExists = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                Utils.ReadWalls(openFileDialog.OpenFile()).ForEach(_walls.Add);
            }
            RedrawDevicesAndHeat();
        }

        #endregion
        
    }
}