﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TiRT_WiFi
{
    public class Wall
    {
        private Line _line;
        private Material _material;

        public int MaterialIndex { get; set; }

        public ScalablePoint Point1 { get; set; }
        public ScalablePoint Point2 { get; set; }

        public Wall()
        {
        }
        
        public Point GetViewPoint1()
        {
            return Point1.GetPixelPoint();
        }

        public Point GetViewPoint2()
        {
            return Point2.GetPixelPoint();
        }

        public Wall(int materialIndex, Point p1, Point p2)
        {
            MaterialIndex = materialIndex;
            Point1 = ScalablePoint.FromPixels(p1.X, p1.Y);
            Point2 = ScalablePoint.FromPixels(p2.X, p2.Y);
            SetMaterial();
            SetLine();
        }

        public Point ToRealPoint(Point viewPoint)
        {
            double diff = Settings.CurrentSettings.Diff;
            return new Point(viewPoint.X / diff, viewPoint.Y / diff);
        }

        public Point ToViewPoint(Point realPoint)
        {
            double diff = Settings.CurrentSettings.Diff;
            return new Point(realPoint.X * diff, realPoint.Y * diff);
        }

        public Line GetDisplayLine()
        {
            if (_line == null)
                SetLine();
            return _line;
        }

        public Tuple<Point, Point> GetRealLine()
        {
            return Tuple.Create(Point1.GetRealPoint(), Point2.GetRealPoint());
        }

        public Material GetMaterial()
        {
            if (_material == null)
                SetMaterial();
            return _material;
        }

        private void SetMaterial()
        {
            _material = Materials.MaterialsList[MaterialIndex];
        }

        private void SetLine()
        {
            Line line = new Line
            {
                X1 = GetViewPoint1().X,
                Y1 = GetViewPoint1().Y,
                X2 = GetViewPoint2().X,
                Y2 = GetViewPoint2().Y,
                StrokeThickness = Settings.CurrentSettings.StrokeThickness,
                Stroke = new SolidColorBrush(GetMaterial().Color)
            };
            _line = line;
        }

        public override string ToString()
        {
            return _material.ToString();
        }
    }
}