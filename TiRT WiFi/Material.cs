﻿using System.Windows.Media;

namespace TiRT_WiFi
{
    public class Material
    {
        public Color Color { get; }
        public double Value { get; }
        public string Name { get; }
        
        public Material(string name, double value, Color color)
        {
            Name = name;
            Value = value;
            Color = color;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}