﻿using System.IO;

namespace TiRT_WiFi
{
    public class Settings
    {
        public static Settings CurrentSettings = new Settings(500, 500, 50, 2, 3);
        public static bool Testing = !false;

        public double Width { get; set; }
        public double Height { get; set; }
        public double Diff { get; set; }
        public double Scale { get; set; }
        public double StrokeThickness { get; set; }


        public static string GetDirectory()
        {
            string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower();
            switch (username)
            {
                case @"dell3-14\3-14":
                    return @"D:\Files\Dropbox\3.14Tom\Semestr 8\TiIRT\test\Piotr";
                case @"desktop-dmcstt2\tomasz":
                    return @"D:\Programy\Dropbox\3.14Tom\Semestr 8\TiIRT\test\Tomasz";
                default:
                    return Directory.GetCurrentDirectory() + @"\test";
            }
        }

        public Settings(double width, double height, double diff, double scale, double strokeThickness)
        {
            Width = width;
            Height = height;
            Diff = diff;
            Scale = scale;
            StrokeThickness = strokeThickness;
        }
    }
}