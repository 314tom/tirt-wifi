﻿using System.Windows;
using TiRT_WiFi.Model;

namespace TiRT_WiFi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            if (Settings.Testing)
            {
                new ResearchRunner().Run();
                Shutdown();
            }
            else
            {
                new MainWindow().Show();
            }
        }
    }
}
